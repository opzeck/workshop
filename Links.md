## Links

[BitLocker - Windows Laufwerksverschlüsselung (ab Windows 10)](https://docs.microsoft.com/de-de/windows/device-security/bitlocker/bitlocker-overview\ "https://docs.microsft.com")  
[Tor Browser (für Mac, Windows, Linux)](https://www.torproject.org/download/download-easy.html.en "https://www.torproject.org/")  
[Thunderbird Emailclient (für Mac, Windows, Linux)](https://www.mozilla.org/de/thunderbird/ "https://www.mozilla.org/")  
[GnuPG Dokumentation](https://www.gnupg.org/index.html "https://www.gnupg.org/")  
[GPG4Win -  GPG-Software (für Windows)](https://www.gpg4win.org/download.html "https://www.gpg4win.org/")  
[GPG-Tools - GPG-Software (für macOS)](https://gpgtools.org/ "https://gpgtools.org/")  
[Sofwarevorschläge für Jabberclients | systemli.org](https://www.systemli.org/service/xmpp.html "https://www.systemli.org/")  
[Tipps für sicherere Kommunikation | systemli.org](https://wiki.systemli.org/howto/tipps_fuer_sicherere_kommunikation "https://wiki.systemli.org/")  
[Jabber-Groupchat mir Pidgin einrichten | systemli.org](https://wiki.systemli.org/howto/jabber-groupchat "https://wiki.systemli.org/")  
