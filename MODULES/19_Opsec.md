## Was ist Opsec?

**Op**erations **Sec**urity beschreibt im weitesten Sinn das Absichern von bestimmten Operationen, Projekten und Prozessen.  
Etwas abstrahiert ist diese Herangehensweise auch anwendbar auf das tägliche Arbeiten in Gruppen und Teams.  
_Opsec sollte nicht ausschließlich in Hinblick auf die technische Möglichkeit, sondern vielmehr unter Berücksichtigung
des für die User noch zumutbaren Aufwands konzipiert werden._

#### Fragen die es zu beantworten gilt:

 * Was muss geschützt werden?
 * Warum?
 * Vor wem muss es geschützt werden?
 * Wann?
 * Wo?
 * Wie ist es am besten geschützt?

Die Frage _»Vor wem muss es geschützt werden«_ sollte möglichst realistisch und präzise beantwortet werden.

#### Weitere Fragen helfen dabei eine realistische Analyse zu erstellen:

 * Welche Möglichkeiten stehen den Angreifern zur Verfügung?
 * Welche Mittel zur Verteidigung gegen die Angreifer stehen zur Verfügung?  
 

### Ein erster Versuch diese Fragen zu beantworten könnte so aussehen:

> **Was?**
>  * (Interne) Kommunikation  
>  * Anonymität  
>  
> **Warum?**
>  * Repression vorbeugen    
>  * Um die Angriffsfläche auf Einzelne Personen möglichst gering zu halten  
>  
> **Vor wem?**  
>  * Polizei und anderen nationalen und Internationalen Repressionsorganen  
>  * Politische Gegner  
>  
> **Wann?**  
>  * Zu jeder Zeit  
>  
> **Wo?**  
>  * Online  
>  * Offline  
>  
> **Wie ist es am besten Geschützt?**  
>  * ???

_Eine genaue Analyse über die Angreifer und ihre Angriffsmethoden ist in diesem Beispiel schwer zu treffen,
da es nur wenige und meist ungenaue Informationen über die aktuellen technischen Möglichkeiten von Polizei,
Verfassungsschutz sowie nationalen und internationalen Geheimdiensten gibt._