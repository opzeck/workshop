# Welche Software verwende ich eigentlich?

Neben der Hardware ist eine ganze Menge Software nötig damit unsere Geräte wie vorgesehen funktionieren.
Software läuft auf den unterschiedlichsten Ebenen unserer Geräte, die bekannteste Software dürfte das Betriebssystem 
sein. Damit das Betriebssystem überhaupt starten kann wird weitere Software auf tieferen Ebenen benötigt. 
Programme die wir selbst installieren setzen auf das Betriebssystem auf und laufen somit auf höheren ebenen.  

Dies kann man sich am besten so vorstellen:

 1. _(Hardware)_
 1. **BIOS**
 1. **Gerätetreiber**
 1. **Betriebssystem**
 1. **Programme & Anwendugen**
 1. _(Benutzer)_

Um bekanntgewordene Sicherheitslücken schnell zu schließen und neue Sicherheitsfeatures ins System zu integrieren 
ist es notwendig  die auf dem gerät verwendete Software, vom BIOS bis hin zu den selbst installierten Programmen, 
regelmäßig zu warten und aktualisieren. Sicherheitslücken in tieferen Ebenen des Systems sind mindestens so gefährlich 
wie jene in der höchsten Ebene.  

## Das Betriebssystem

**Auf der Ebene des Betriebssystems gibt es wenig falsch zu machen, im wesentlichen solltest du drei 
Grundsätze beachten:**
 * Installiere nur Betriebssysteme welche du direkt vom Hersteller bezogen hast und überprüfe nach Möglichkeit 
 die Prüfsumme sowie Softwaresignatur bevor du das Betriebssystem auf einem Computer installierst.
 * Verändere keine Einstellungen dessen Funktion und Tragweite du nicht genau kennst. Dies betrifft insbesondere 
 Einstellungen in der Windows Registry und in den Kommandozeilen Terminals von macOS und Linux.
 * Aktualisiere dein Betriebssystem regelmäßig oder erlaube dem System dies automatisch zu erledigen
 
 
 ## Eigene Programme und Anwendugen
 
Auf dieser Softwareebene werden die meisten Systeme kompromittiert, schau bei Gelegenheit in die Kapitel 
[Gerätesicherheit Computer](02_GeraetesicherheitPC.md) und 
[Gerätesicherheit Smartphones & Handys](03_GeraetesicherheitSmartphonesHandys.md).  

Am häufigsten wird Schadsoftware durch aktive Mitarbeit des Users auf dem Gerät installiert, dies bedeutet das der User 
mindestens ein mal das Ausführen des Schadprogramms durch eine Aktion bestätigt. Häufig wird Schadsoftware in 
Programmen und Dateien versteckt die auf den ersten Blick unverfänglich aussehen. Nicht selten wird Schadsoftware 
in Programmen versteckt die mehr Sicherheit suggerieren, z.B. Anti-Virus Software, Ad-blocker oder andere Browser Plugins.

**Um die gängigsten Infektionswege auszuschließen solltest du versuchen dich an folgende Grundsätze zu halten:**  

 * Installiere nur Software aus vertrauenswürdigen Quellen (Original Website der Entwickler*innen, App Stores)
 * Meide Downloads von Drittanbietern wie chip.de, giga.de, computerbild.de [...]
 * Überprüfe nach dem Download nach Möglichkeit die Prüfsumme und Softwaresignatur der Datei
 * Verändere keine Einstellungen dessen Funktion und Tragweite du nicht genau kennst. Dies betrifft insbesondere 
 sicherheitsrelevante Einstellungen
 * Speichere und öffne keine E-Mail Anhänge von unbekannten Absender*innen
 * Aktualisiere deine Programme und Anwendungen regelmäßig oder erlaube ihnen dies selbstständig zu tun
 * Installiere keine Plugins oder Add-Ons von unbekannten Drittanbietern
 * Installiere einen renomierten Ad-Blocker in deinem Web Browser. 
 (z.B. [uBlock **Origin**](https://de.wikipedia.org/wiki/UBlock_Origin))
 * Nutze keine "Raubkopien" (Programme, Videos, Musik, E-Books, etc...)
 * Nutze keine Software zum (illegalem) aktivieren von Programmen (Cracks, Key Generators, etc...)
 * Vermeide Websites mit illegalem Video-Streaming (Filme, Live Streams, Pornos)
 * Wenn du in einem Terminal arbeitest, verwende nur Programme/Befehle dessen Funktion du kennst. 
 Vermeide das Ausführen langer Befehlsketten aus Anleitungen und Tutorials wenn du diese nicht verstehst
 