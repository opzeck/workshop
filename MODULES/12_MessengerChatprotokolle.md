# Instant Messenger & Chat-Protokolle

[Nachrichtenverschlüsselung](24_NachrichtenVerschluesselung.md) mit [GnuPG](22_GnuPG.md) ist auf Smartphones zwar
möglich, in der Einrichtung und Anwendung jedoch unverhältnismäßig kompliziert.  
Es existieren einige Programme die sich für Kurznachrichten und einen mobilen Anwendungszweck besser eignen und unter
Umständen als "sicherer" angesehen werden können, da sie die an Verschlüsselungssoftware gestellten Kriterien (siehe
[Nachrichtenverschlüsselung](24_NachrichtenVerschluesselung.md)) besser erfüllen.  

Neben einer vereinfachten Usability werden außerdem einige Features geboten welche dazu geeignet sind, digitale
Kommunikation langfristig sicherer zu gestalten.  
Bei der Verwendung von "Crypto-Messengern" ist in Jedem Fall zu beachten, dass die empfangenen Nachrichten unter
einigen Umständen unverschlüsselt auf dem Gerät gespeichert und bei fehlender Geräteverschlüsselung ausgelesen
werden können.

#### Trotz hoher Sicherheitsversprechen der Crypto-Messenger variiert die effektiv gebotene Sicherheit stark.

## Signal
Signal ist Open Source Software und kostenlos auf allen mobilen Plattformen verfügbar. Für Windows, macOS und Linux wird
außerdem eine Desktop Version bereitgestellt.  
Eine Anmeldung erfolgt anhand der Telefonnummer, Gruppenchats werden ebenso wie normale Nachrichten standardmäßig
verschlüsselt.  
Zur Verschlüsselung der Nachrichten wird das gleichnamige
[Signal-Protokoll](https://de.wikipedia.org/wiki/Signal-Protokoll "https://de.wikipedia.de") verwendet. Dieses wurde
bereits mehrfach auf Schwachstellen überprüft und gilt als anerkannt.

## Threema
Bei Threema handelt es sich um proprietäre Software welche auf allen mobilen Plattformen verfügbar ist. Die einmaligen
Kosten der App liegen bei ca. 2€. Über einen Web-Browser ist es außerdem Möglich Threema auf allen Desktop-Plattformen
zu nutzen.  
Zur Anmeldung wird keine Telefonnummer benötigt, zur Authentifizierung wird eine zufällige ID generiert. Gruppenchats
werden ebenso wie normale Nachrichten standardmäßig verschlüsselt.  
Zur Verschlüsselung der Nachrichten wird die quelloffene Programmbibliothek "NaCl" verwendet. Dieses wurde bereits
mehrfach auf Schwachstellen überprüft und gilt als anerkannt.  


## WhatsApp
WhatsApp ist ein kostenloser proprietärer Messenger und gehört zum Facebook-Konzern. WhatsApp ist auf allen Plattformen 
verfügbar. Für Windows, macOS und Linux wird außerdem eine Desktop Version bereitgestellt.  
Eine Anmeldung erfolgt anhand der Telefonnummer, Gruppenchats werden ebenso wie normale Nachrichten standardmäßig
verschlüsselt.  
Zur Verschlüsselung der Nachrichten wird das Signal-Protokoll verwendet. Dieses wurde bereits mehrfach auf
Schwachstellen überprüft und gilt als anerkannt.  

Die Verschlüsselung der Nachrichten gilt als sicher, jedoch bestehen wie bei allen Diensten von Facebook erheblich
Bedenken hinsichtlich des Datenschutzes.

## Telegram
WhatsApp ist ein kostenloser proprietärer Messenger und auf auf allen mobilen Plattformen verfügbar. Für Windows, macOS
und Linux wird außerdem eine Desktop Version bereitgestellt.  
Eine Anmeldung erfolgt anhand der Telefonnummer.  

**Telegram bietet keine Ende-zu-Ende Verschlüsselung für Gruppenchats, normale Nachrichten werden standardmäßig nicht
Ende-zu-Ende verschlüsselt!**  
Um einen verschlüsselten Chat zu starten muss vorher ein sog. "Geheimer Chat" initiiert werden.  
Zur Verschlüsselung der Nachrichten verzichtet Telegram auf anerkannte Standards und setzt auf das selbst entwickelte
MTProto-Protokoll. Die Sicherheit dieses Protokolls, des Gesamtkonzepts, sowie die Vermarktung als „sicherer Messenger“
wurden von Experten vielfach kritisiert.

## XMPP/Jabber
### OMEMO / OTR