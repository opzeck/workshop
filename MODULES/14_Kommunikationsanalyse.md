# Kommunikationsanalyse

Die Kommunikationsanalyse ist eine Methode zum abbilden komplexer Zusammenhänge mit Hilfe von gesammelten Metadaten.  
Anhand der analysierten Daten ist es möglich, nahezu fehlerfrei Gruppenzusammenhänge, Personen- und Freundeskreise 
sowie "Bubbles" aufzudecken.  

Um eine Kommunikationsanalyse durchzuführen bedarf es keinerlei Kenntnis über den Inhalt der zu analysierenden Nachrichten, 
eine Nachrichtenverschlüsselung bietet daher keinen Schutz vor einer deanonymisierung durch eine Kommunikationsanalyse. 
Je größer der analysierte Datensatz ist, desto präziser wird das Ergebnis der Kommunikationsanalyse.
Für eine Kommunikationsanalyse können (Verbindungs-) Daten von **Telefonaten**, **E-Mails**, **Snake-Mails (Post)**, 
**Instant Messaging** oder unterschiedliche Datenbestände aus **Sozialen Netzwerken** herangezogen werden.

Gegenmaßnahmen wirken nur in Kombination und unter strikter Einhaltung von Nachrichtenverschlüsselung und 
Methoden zur anonymisierung [(im Internet)](07_AnonymitaetImInternet.md).  

## Beispiel einer Kommunikationsanalyse

Dieses Beispiel zeigt die Analyse einer konspirativ agierenden Gruppe auf Basis einer Auswertung der Kommunikationsdaten 
von wenigen Mitgliedern.  
**Für unser Beispiel geben wir der Gruppe den Namen "Muppet Group", abgekürzt "MG".**  

Als Ausgangslage ist bekannt, dass **_Anton_** und **_Beatrice_** zur "MG" gehören.

<div align="center">
  <img src="../IMG/kanalyse1.png" alt="Anton & Beatrice" width="500"/>
</div>  

###
_Durch Auswertung aller zur Verfügung stehenden Kommunikationsdaten von **Anton** und **Beatrice** erhält man ein 
umfangreiches Netz ihrer sozialen Kontakte. Dabei wird nicht nur einfach die **Anzahl der Kommunikationsprozesse** 
ausgewertet, es wird auch die **zeitliche Korrelation** einbezogen._  

<div align="center">
  <img src="../IMG/kanalyse2.png" alt="Anton, Beatrice & friends" width="500"/>
</div>   

###
_Besonders **häufig haben beide (zeitlich korreliert) Kontakt zu Clementine und Detlef**. Diese beiden Personen 
scheinen eine wesentliche Rolle innerhalb der Gruppe "MG" zu spielen. Einige Personen können als offensichtlich 
privat aus der weiteren Analyse entfernt werden, da nur einer von beiden Kontakt hält und keine zeitlichen 
Korrelationen erkennbar sind._   

_Ideal wäre es, an dieser Stelle die Kommunikation von **Clementine** und **Detlef** näher zu untersuchen. 
Beide sind aber vorsichtig und es besteht kein umfassender Zugriff auf die Kommunikationsdaten. 
Dann nimmt man als Ersatz vielleicht **Frida**, um das Modell zu präzisieren._  

<div align="center">
  <img src="../IMG/kanalyse3.png" alt="The Muppet Group" width="500"/>
</div>  

###
_**Frida** unterhält vor allem einen engen Kontakt zu **Detlef**, was zur Umbewertung der Positionen von **Detlef** und 
**Clementine** führt. Bei **Emil** handelt es sich evtl. um einen zufällig gemeinsamen Bekannten von **Anton** und **Beatrice**, 
der nicht in die "MG" eingebunden ist._ 
 
 **Aktuelle Forschungsergebnisse zeigen, dass man nur die Kommunikationsdaten von 8-10% der Mitglieder einer 
 Gruppe auswerten muss, um ein nahezu vollständiges Abbild der Struktur einer Gruppe zu erhalten.**  
 
 
 ## Reale Kommunikationsnetze
 
 Reale Kommunikationsnetzwerke sind etwas komplexer. Auf Grundlage der Daten, die von T-Mobile über den 
 Politiker Malte Spitz gespeichert wurden, hat Michael Kreil von OpenDataCity die folgende Grafik mit den 
 Rohdaten erstellt.
 
 <div align="center">
   <img src="../IMG/cluster1_small.png" alt="Kommunikationsnetzwerk von Malte Spitz" width="500"/>
 </div> 
 
 ###
 ## Kommunikationsanalyse mit öffentlichen Daten aus Sozialen Netzwerken  
 
 Auch mit öffentlich zugänglichen Daten lassen sich innerhalb kürzester Zeit Gruppenstrukturen abbilden. Im folgenden 
 Beispiel analysieren wir einen winzigen Datensatz einer Antifa Gruppe.  
 Die Gruppe betreibt eine eigene Seite in einem Sozialen Netzwerk. Für unseren Versuch gucken wir uns die letzten 15 
 veröffentlichten Beiträge etwas genauer an.  
 
 Als Datengrundlage nutzen wir die öffentlich einsehbaren Reaktionen (Likes) der Beiträge.  
 _Der Datensatz wurde anonymisiert._   
 
 
<img src="../IMG/Kommunikationsanalyse/PNG/H-1.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/H-2.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/H.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/HAOCMAJ.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/HAOM.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/HCA.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/HD.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/HOCA.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/A.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/AC.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/AM.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/O-1.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/O.png" alt="Kommunikationsanalyse Facebook" width="200" />
<img src="../IMG/Kommunikationsanalyse/PNG/OJC.png" alt="Kommunikationsanalyse Facebook" width="200" />  

###
###

**_Nun halten wir nach Mustern, bzw. wiederholt gelisteten Namen ausschau, daraus ergibt sich eine kleine Liste:_**

 * **Hartmut Dudde** _(9)_
 * **Andy Grote** _(7)_
 * **Olaf Scholz** _(6)_
 * **Christiane Schneider** _(5)_
 * **Martina Friedrichs** _(4)_
 * **Andreas Dressel** _(3)_
 * **Juliane Timmermann** _(3)_

Das sind die Accounts welche besonders häufig mit den letzten 15 Beiträgen der Antifa Gruppe interagiert haben

**Im Gespräch mit der analysierten Gruppe stellte sich heraus, dass 6/7 aktuelle oder ehemalige 
Mitglieder der Struktur sind.**  
Auf gleiche Art lassen sich auch Interaktionen anderer Sozialer Netzwerke analysieren, ganz gleich ob es sich um 
comments, likes, shares, favs, herzen, mentions o.Ä. handelt.
