# Linke, Datensparsame Infrastruktur

Linksradikale Technikkollektive betreiben verschiedene Services unf bieten diese für Aktivist*innen an.
Neben E-Mail Postfächern die auf eigenen Servern betrieben werden, gibt es viele unterschiedliche Dienste die das 
Arbeiten in Gruppen vereinfachen und dabei einen großen Wert auf Datensparsamkeit, Sicherheit & 
Open-Source-Software legen.  

Es gibt gute Alternativen zu Diensten wie Google Drive, Dropbox, Doodle, Skype & kommerziellen VPN’s. 
Außerdem können eigene Foren, Wikis & Mailinglisten erstellt & benutzt werden.


## [www.systemli.org](https://www.systemli.org)

> systemli.org ist ein unkommerzieller Anbieter für datenschutzfreundliche Kommunikation. Ganz ohne Überwachung.  


 * [E-Mail Adressen](https://www.systemli.org/service/mail.html) mit 1GB Speicherplatz, Dateianhänge bis 40MB _(nur mit Invite-Code)_
 * [Jabber/XMPP](https://www.systemli.org/service/xmpp.html), Offenes Messaging-Protokoll
 * [Etherpad](https://www.systemli.org/service/etherpad.html), Online Texteditor für kollaboratives Arbeiten an Texten
 * [Ethercalc](https://www.systemli.org/service/ethercalc.html), Online Tabellenkalkulation für kollaboratives Arbeiten an Tabellen
 * [Paste](https://www.systemli.org/service/paste.html), Teilen von verschlüsselten Texten mit Verfallsdatum
 * [Croodle](https://www.systemli.org/service/croodle.html), verschlüsselte Umfragen zur Terminfindung
 * [Mumble](https://www.systemli.org/service/mumble.html), verschlüsselte Telefonkonferenzen
 * [Private Foren](https://www.systemli.org/service/hosting.html), zur Organisation & Diskussion _(auf Anfrage)_
 * [Private Wikis](https://www.systemli.org/service/hosting.html), zur Organisation & Wissensverwaltung _(auf Anfrage)_
 * [Metadaten Cleaner](https://www.systemli.org/service/metadata.html), entfernt Metadaten aus Mediendateien
 * [Webhosting](https://www.systemli.org/service/hosting.html) für Websites und Blogs _(auf Anfrage)_
 * [Demoticker](https://www.systemli.org/service/ticker.html), WAP-fähiger Kurznachrichtendienst _(auf Anfrage)_ 

_[systemli.org auf github.com](https://github.com/systemli "https://github.com")_

## [riseup.net](https://riseup.net)

> riseup.net ist ein Projekt für demokratische Alternativen und praktische Selbstbestimmung durch Kontrolle 
unserer eigenen sicheren Kommunikationsmittel. Es ist ein kleines anarchistisches Technikkollektiv in Seattle.


 * [E-Mail Adressen](https://account.riseup.net/user/new) _(nur mit Invite-Code)_
 * [VPN](https://riseup.net/de/vpn/vpn-red) (Windows, macOS, Linux)
 * [Unverschlüsselte Mailinglisten](https://riseup.net/de/lists)
 * [Private Wikis](https://we.riseup.net/)
 * [Gut gefülltes Wiki zu Kommunikationssicherheit](https://riseup.net/de/security)

## [www.immerda.ch](https://www.immerda.ch)


> immerda.ch ist ein autonomes Kollektiv, das Email, Listen, Webspace für Freund*innen anbietet. Sie unterstützen 
> progressive und emanzipatorische Gruppen und Individuen mit dem Ziel, die Welt zu verändern. Die Dienste werden 
> angeboten für Befreundete von immerda, geknüpft an Einladungen.

 * [E-Mail Adressen](https://www.immerda.ch//infos/email.html)
 * [Jabber/XMPP](https://www.immerda.ch/infos/im.html)
 * [Schichtplan](https://schichtplan.immerda.ch/), zum Koordinieren von Helfer*innen für Veranstaltungen
 * [Verschlüsselte Mailinglisten (Schleuder)](https://www.immerda.ch/infos/lists.html)
 * [git repositories](http://git.immerda.ch/)
 * [GPG Keyserver](https://keys.immerda.ch/)
 
 ## Weitere Angebote linker Infrastruktur
 
 [Radikale Server](https://riseup.net/de/security/resources/radical-servers#systemliorg "https://riseup.net")