# Digitales Arbeiten in Teams

> _Die beste Nachrichten-Verschlüsselung hilft nicht viel wenn die verwendeten Geräte leicht angreifbar, 
> unsicher oder bereits kompromittiert sind.  
> **Das »unsicherste« Gerät bestimmt daher die Sicherheit der ganzen Kommunikation.**_


## Gruppen E-Mail Adresse

### Mail Client Konfiguration

#### IMAP
#### Ordner und Filter
#### Gemeinsam einen GPG Private Key verwenden
#### Mailing Listen (Schleuder)


## Groupware

#### Matrix.org & Riot.IM
#### Pads, Wikis & Foren
#### Nextcloud