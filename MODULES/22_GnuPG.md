# GnuPG, PGP
**GNU Privacy Guard** _(englisch für GNU-Privatsphärenschutz)_, abgekürzt **GnuPG** oder **GPG**, ist ein freies Kryptographiesystem. 
Es dient zum Ver- und Entschlüsseln von Daten sowie zum Erzeugen und Prüfen elektronischer Signaturen. 
Das Programm implementiert den OpenPGP-Standard und wurde als Ersatz für PGP entwickelt. 
Versionen ab 2.0 implementieren auch den S/MIME- und den PGP/MIME-Standard. GnuPG benutzt standardmäßig nur patentfreie 
Algorithmen und wird unter der GNU-GPL vertrieben. Es kann unter GNU/Linux, MacOS und diversen anderen unixoiden 
Systemen sowie unter Microsoft Windows betrieben werden.  

## Benötigte Software
#### Microsoft Windows
 * [**Gpg4win**](https://gpg4win.de "https://gpg4win.de")

#### macOS
* [**GPGTools**](https://gpgtools.org "https://gpgtools.org")

#### Linux
* _GnuPG ist auf den meisten Linux Distributionen bereits installiert_

#### Smartphones
_Zumindest auf Android Smartphones ist es mit zusätzlichen Apps möglich Nachrichten mit GnuPG zu Ver- und Entschlüsseln,
in der praktischen Anwendung ist dies jedoch sehr umständlich, [Instant Messenger](12_MessengerChatprotokolle.md) 
wie z.B. Signal sind für die mobile Kommunikation das Mittel der Wahl_

## GPG Verschlüsselung bietet
 * **Vertraulichkeit**  
 _unsere Nachrichten sind verschlüsselt & nur für die Empfänger\*innen lesbar_
 
 * **Beglaubigung**  
 _wir können die Authentizität der Absender\*innen sicherstellen_ 


## GPG Verschlüsselung bietet nicht
 * **Keine Abstreitbarkeit der Kommunikation**  
   _Wer, mit wem, von wo & zu welchem Zeitpunkt kommuniziert ist kein Geheimnis_  
   
 * **Keine Folgenlosigkeit**  
   _Falls ein Private Key verloren geht oder die Verschlüsselung geknackt wird_


## Funktionsweise, Asymetrische Verschlüsselung
Asymetrische Verschlüsselung beschreibt eine Verschlüsselungsmethode bei der für die Entschlüsselung **kein gemeinsames
Geheimnis** notwendig ist. So ist es möglich eine verschlüsselte Korrespondenz zu beginnen, ohne vorher mit allen 
Beteiligten ein Passwort oder eine Chiffre vereinbart zu haben.  

Statt eines gemeinsamen Geheimnisses nutzen asymetrischen Verschlüsselungsverfahren jeweils **eine öffentliche**, 
sowie eine **private Information** um Daten zu Ver- und Entschlüsseln. **Die private Information bleibt dabei 
immer privat und wird nicht mit anderen geteilt**.  
Bei asymetrischer Verschlüsselung werden diese beiden Informationen Schlüssel (keys) genannt, 
konkret: **_Private Key_** und **_Public Key_**.  

Bei den beiden Schlüsseln handelt sich um ein **Schlüsselpaar** welches sich direkt aufeinander bezieht.

**Public Key**
 * Der **Verschlüsselungsalgorithmus** erzeugt aus einem Klartext unter Verwendung des öffentlichen Schlüssels 
 einen Geheimtext.  


**Private Key**
 * Der **Entschlüsselungsalgorithmus** berechnet zu einem Geheimtext unter Verwendung des geheimen Schlüssels den 
 passenden Klartext.  

Es wird nun gefordert, dass jede Nachricht, die mit einem öffentlichen Schlüssel verschlüsselt wurde, mit dem 
zugehörigen geheimen Schlüssel wieder aus dem Chiffrat (dem verschlüsselten Text) gewonnen werden kann.
 
<div align="center">
  <img src="../IMG/PrivateKeyPublicKeyEncryption.png" alt="GPG Keys zur Ver- und Entschlüsselung von Nachrichten" width="350"/>
</div>  

_Bevor du eine verschlüsselte Nachricht verschicken kannst brauchst du also den Public Key des Empfängers der Nachricht.  
Der Empfänger deiner Nachricht kann diese dann mit seinem Private Key entschlüsseln und sofern er im Besitz _deines_ 
Public Keys ist, ebenfalls verschlüsselt auf deine Nachricht Antworten._


## Public Keys & Schlüsseltausch
Bevor du eine verschlüsselte Unterhaltung beginnst, brauchst du den aktuellen Public Key deines Gesprächpartners. 
Falls du deinen Gesprächspartner vorab kennst, kannst du ihn persönlich um den Public Key bitten. Oftmals bleibt 
nichts anderes übrig als in einer unverschlüsselten E-Mail um den fehlenden Schlüssel zu bitten. Bestenfalls hängst du deinen 
Public Key der E-Mail an, so kann dein Gesprächspartner bereits verschlüsselt antworten.

## Schlüsselserver
Um das Arbeiten mit GPG Keys zu erleichtern, gibt es sog. Key Server. Auf diesen Servern kann man seinen Public Key
hinterlegen und für die Öffentlichkeit zugänglich machen. Natürlich kann man auch nach den Public Keys anderer Leute 
suchen um diesen ohne vorherigen Schlüsseltausch verschlüsselt zu schreiben. Es gibt viele verschiedene Schlüsselserver,
diese sind miteinander vernetzt und tauschen regelmäßig die hochgeladenen Public Keys untereinander aus.  

**Wird ein Public Key auf einem Schlüsselserver hochgeladen, kann man diesen danach nicht wieder vom Server entfernen.**


## Signaturen
Neben dem Verschlüsseln von Nachrichten, kann man mit dem Private Key auch eigene Nachrichten signieren.
Um eine Nachricht zu signieren braucht man neben dem eigenen Privat Key auch die dazugehörige Passphrase. 
Das signieren von Nachrichten dient ähnlich einer analogen Unterschrift der Authentifizierung deiner Identität.  

**Signierte Nachrichten sind nicht automatisch verschlüsselt und verschlüsselte Nachrichten nicht automatisch signiert.
Beide Funktionen können einzeln oder kombiniert genutzt werden**


## Beglaubigungen & Web of trust
Es ist möglich mit dem eigenen Private Key fremde Public Keys zu beglaubigen. Diese Beglaubigungen sind über die 
Schlüsselserver einsehbar. Diese Funktion wird verwendet um ein »Web of Trust« und sog. »Trust Chains« zu erzeugen.
Im Prinzip teilt man anderen Benutzern so sein Vertrauen in den beglaubigten Schlüssel bzw. die beglaubigte 
Identität mit.  

**Achtung: Da die Beglaubigungen öffentlich einsehbar sind können sie zu einer Deanonymisierung führen!**


## Ablaufdatum & Widerrufszertifikate
Mit einem Widerrufszertifikat kann die Gültigkeit eines GPG Keys widerrufen. So ist es möglich anderen über 
Schlüsselserver mitzuteilen dass dieser GPG Key nicht mehr benutzt wird.  

Dies bietet sich insbesondere nach einem Verlust des Privat Keys oder der Passphrase, einer Kompromittierung 
des GPG Keys oder einer Beschlagnahmung von verwendeten Geräten an.
Ein Widerrufszertifikat wird auf dem selben Weg wie der Public Key auf einen Schlüsselserver geladen. 
Um ein Widerrufszertifikat zu erzeugen wird die Passphrase des GPG Keys benötigt. 
Widerrufszertifikate können direkt nach dem generieren des GPG Keys erstellt werden und sollten sicher verwahrt werden.  

Außerdem ist es möglich ein Ablaufdatum des GPG Keys festzulegen. Das Ablaufdatum kann jederzeit, auch bei 
bereits abgelaufenen GPG Keys verlängert werden.  


## Ex- & Import von GPG Keys
Das Ex- & Importieren einzelner Schlüssel oder des kompletten Schlüsselbunds ist ohne weiteres möglich.
Bei einem Umzug auf einen neuen Computer vermeidet man so das ständige Wechseln der eigenen Schlüssel sowie das 
sammeln der Public Keys seiner Kontakte. Es ist möglich den selben Schlüssel auf mehreren Geräten und mit 
unterschiedlichen Passphrasen zu verwenden.  
So ist es möglich mit mehreren Personen gleichberechtigt eine Gruppen E-Mail Adresse zu verwalten oder den 
eigenen GPG Key auf mehreren Geräten zu nutzen.
 
Übertragt den Privat Key mit einem USB-Stick auf ein anderes Gerät und löscht den Stick danach mehrfach.  
**Ein Privat Key hat nichts im Internet verloren!**


## PEP (pretty easy privacy)
(tbc)

## Wie ist ein GPG Public Key aufgebaut?
Schlüssel-ID: `E3DE0EE9`  
Fingerabdruck: `0910 E63C F5C0 3FA5 48F2  05CB 5BFF 96AC E3DE 0EE9`

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFqxaoYBEADR7MTL0Nq2FPyNneBdrPi6HKU8+IElVRihv9xl9yGZO/7ehsuP
nZ1mvVP3mqiEDF+tCdfU0oLFj+e1njfkdrXXgA1XX6dBN8XAyx2iIEXuFk1zPQGQ
d9MWHnegXemaBBRmr2W3Vvk7MSu4jFUGWPtyrMuvUSZBDJlO2U2vstWZCaTPLSf7
tqLaPNUzgujo9rmBoP234oapokjo95FbYPsTYMoO4F8eceN75ArZUFNWb1+jujPy
2+noAQnHYPn2gyhi2QYXnSiRl4V4dclWRM9XXqKZL+exGTq3zclCHZwNo3Ge1CQG
rrLZLOdBELbmAjtBzp/TkO2sLihapDhH3+WaSluuxnWtoOqz4/PIxXD6VmxOtRkp
5wq4NV4jvRRB4OQQHXr0IthJYhFcJoXNtG9v1AKPCQyYY65Ma9hsfyEYCYVMY0Kv
7E2eARlY/NauZriVLWwLGANSgtFD0e24/4g3uwZNwPC328mqzdazSI7N6JSph4Vq
opfn0NKkJv5Pz5p8FKeMj9lBovBz7pgC5z6Xh6FgKd9Gd6zvMwIsvrrAF0SB0Hzd
f0TuwAlJnVAW+XZOe0lFctmBthtMbR/3TpJYYckR+Ks5wFb4kMOuoOp/1EDBT2or
ZLT7DcaSzhvrbaqpCoa3T8HN81B/vCy30b61DfUGM5HaiskqQHKxrOwyYwARAQAB
tBxvcHplY2sgPG9wemVja0BzeXN0ZW1saS5vcmc+iQJUBBMBCgA+AhsjBQsJCAcD
BRUKCQgLBRYCAwEAAh4BAheAFiEECRDmPPXAP6VI8gXLW/+WrOPeDukFAlsculYF
CQNv3tAACgkQW/+WrOPeDunLVhAAmtKFh4MlBmEOKezcDqIOsm8Z/nlHxKkxIgTH
KAYM5NQfL/B+dmV7TeH3DaxsRk8cxGflxmIL12Z6dLQjjw/tA1JW7PA+bBSAzKr0
HLTLBP1crtSJhzmI5o8hfwAYJbuWZbiKbKzlePkOnVP04Ik/5szXq0ZP/JReYn29
w/8KpZkJlk/fG+lCeV/YkjqGEwWGEgSyrVIHQ+PSVSont8BqYcEBOc2+iIhl8SK0
c3vfZ2VYFn+870sBKx2vNgKCJ5U/oyUxgTH2FoG+c7NLwyE9D/zZhVDD9FA5snSQ
NsqEnxpCJ9E/yY/WUNf0gaXPKE5u5BhAAUUpbTLDC+hoqLZ1rShdm4WeHLhBuA18
QqiW2gT+gn7UAqAU3zTCpelF6t/oUzJtao8QITnXkEekmUOTnxUo2AwEsWMu7z8f
pCihK1gE16ODIuSDlDzHldtkZJjGfap7D0tY9tMYsRsxMTLBD8cCTJ/YHf52DKZ+
ltp7MFo4A8xjhHjQJjhJO4KCgU9bDxyeoe5rHlRR4tsTH8ALESBUoNZW9+lU/I4X
XIIInj9/Io8bpQ5YHSsopGYjKkb4HzbcfajaBH1Edt/GgAAv1aNRvugQC1o1YEPQ
DeLm2c7r7lrwcJAxOlRZsHuzgH/pFwP2274OmhWnHKBc4KGjxkxp584nhFDV+o/g
p1uSrd65Ag0EWrFqhgEQALlguxM6I8jFQs4RcOPaPrURNilf01gYd/+YcAy1tpZw
TQJcYpG4KedkZ6YN2DWpWGRrYBANKwwKPDqXtFm6RvwcjSDsbBoYxJeitqtgbD2Q
v7rlR14RgG9Hli6nUrWdhYfSqPLNMxyt03xXlORsC3sOGw5Hyf4fI0d+BYeFcEcm
RKjKMr5444ooxwSzErL+IDB0LbZxaLaaX2OxcA4BAOr/6X823rZZEDjj9tLz3SoB
3YRBEi/LeJr0hJ9GyYWSLXc1AnlO2g03yaVoUvVAZiAb2zc86iL0uft0B2QuGptk
E3SWUnsLEFXkXdUC9jxJ198du2tWlLe/gevPn2Rg3qfMuntJIvQCkIqUyG9v32VE
1EdWsOJTD0RLuVe7PBkhe0sxgBYAK0C1rScjqNCJdaj2uxcOyqFva0Gy43NHa+1D
PaHPaPIhYXMVLxCcBDLMAxM9SexR5L9zuQv6MDo5M0myU4ppwKtenSLDwZACapGE
/7r7AAlKGB+a6Qg6C3TP60lyegnfxLIT8HM2QNuC5t29Ro84pyx5oJ6V/Wk+LuFr
6P+2ku2fJaSG4mmm3jHhcHy9mHDIlMnC6nszD9lAZp4cfaemTvx9+lHTGwyVq/vF
gCIRPiw0hMUGiWaxYLr7LOIgPLyIraeZurVkfvbLLtkr2R5gSvw952LQcH0rzS7t
ABEBAAGJAjwEGAEKACYCGwwWIQQJEOY89cA/pUjyBctb/5as494O6QUCWxy6wwUJ
A8LtvQAKCRBb/5as494O6QOkEADF1TfBMgvT5tf83IvWt6Xd5ff41f8yi6j/dHci
bpQvdnjq/ld0hh+kuSNhzTtwxG2mgTC/UFOP2rYDLEosHinzrGkqIKaR1DkVn+aX
HpH37zGpA5Yr2PRSslrym22qKem9d3hpBj1Ml+IrbyWCVT5CVZggvT21kuLbUSfD
f5nGQoF1Sv/48Q7D1gdc73YCrWzsEkMFfCv4MNwGD6C1ENHDwrsLhwgTDKIR2+dS
CaBZLaf6J4e0Q2WGpaS90ThLw8lEy7SSatjwOKV14oKiODBsLg6nNxHe2PYCKuS3
J4YmnpzKVIF917tF9stJ8PUC8Ee3w1eNuWeD7qQKme79EyZygyuOxKHx+QGxQ0Ms
FL/pyLz6UQJYRXEBfouy+2oSBCXWu6hObwYaYB1HdUHoBpF/qZ+3Insfpk8imVW4
pHK9h+qVHMhpFoovNEsWZirXSvx9aYGyEzMnFrpmKuD8MwvmfhhabGpI3xWcJepM
cIwdbLj9QQ3rsp3dn4WJCZ0kbZzfdcmWU9cgZtU4OLcL4ugd8qMIaaNJPjC08A9y
OeCQQ6HtcsV30dBgmU6XEfQnXXW8OZMhWZVy0SSAFHoXOK0OswvBGvm4B2JmLA7O
TIPmrnYhk6x7vcv/SSGJj0ev6OyjZ+r/yk7KePCESmxU/mJ+gNV0LlPz3FJ5VhXE
0xlSALkCDQRbHLqEARAAxyMzbJX8vRz18qUSLz0k1UOn5eoAuIOXLqXvPCM8YfH1
/QzSHdfWzbuPXIkcEzhDCZjDcc8o228/riskSWcMt2neP8erO1A1rDg1FpvPiTJP
gU3KBVV02D6xNMvUvRaPX71Bu865ee4LBEcA2kfQgtyUYKYgpkguswlombwnWSDE
F755ThouADjnRE4PL9Ed1xLAIi9gmYsdw9wOEu9hvs7kOCVH5ZDWVP46OQr81aSk
KAQ1iOj7gxGjxJGa7lPlaisO4uem1b87q690OH6+4hk8y1vM77JYVGcHO6j+HCTY
H8Nu5d3Z5AokeyFVxS/fGCh7B5vCFwSez5mUNq0gJhCXEuS3Ab2VYfSonmFT74dU
tBnznwNCa1y8nDkdg7MLSX5aFWtVTDT0OsZA5sh/9STECk4jm8v9LsOJkQDxypj+
WyGk+LQYZaJXI5SObhJdCClszK7zt4TxGhIre8ZSMepoEx78gM4x0GErY02vUBUY
8Tsx/6MYbKKWnBe8ZsxCBaVgvMcaU+u5KVJSXcs9l4zxNtn22N2j7rs2T3ym6FnZ
SqprzwtwmDbq1G9cBOvjOttre01QL5LOueMgx1Rv5doSRzWg5IWNyVVPcFhYYc8l
uTEkliF9isy78/GzlaNk31cFlqALoLLF/Xxbz1nq1ZLYSqElm867eOV7pYDEU4sA
EQEAAYkEcgQYAQoAJhYhBAkQ5jz1wD+lSPIFy1v/lqzj3g7pBQJbHLqEAhsCBQkD
w7iAAkAJEFv/lqzj3g7pwXQgBBkBCgAdFiEEcUJhUYJk5f4IyaJ6ZIfM6V29mzoF
AlscuoQACgkQZIfM6V29mzomAw/7BoimOiS+eu9s5CkWortyWMiwuLqBrBXyq7ub
X6MUZyPPDvfuC5QO9hN2SbWP12DZ6RVxqeKlycMBAViy5HzUxzRtxTlAauQur9I2
/cZ5Nlyw3XpTZtCI4xv0y4mwsm9PwrxhYo1++t5ujkbyy6OlGTsFAY5kF3ODubEK
mjJSTa2U1us3eiteYkf+xaYxp5lE/j2sYFZw8hssIEHG+5KgvIije8WNSi1qGdHk
Gk8EtnaWQrtpYPRFx03fs+pPeWkYi/aICVvsUcsFWuYUaqKOh8scN1wi8cdgzxWe
b+ie0OkztTPy4EIW4HOvOJ7Et/auaRhrPVmskluKNu+VkFMSedhxtkuS1+clbcDF
6wquj5POzh3j19u/5LucSGV0g3jju2WNysQ2YkLtQvouAb2HOP2L307d7WXtBr7f
zm6k4iCTkbs+qRVr3l7ZTIe4OkFNG8GlyQZvk8khywSu9mEjXsu3eSUMaaCn2IDw
/1WVOTte1rydspIn0shS/uLwPSlKsaBZJb6FNLjHufsHMFRVeQye7pW6qiVYW7Mr
OK/AyyT0OAvYL1+hUX2meRKJtySbiEmx+4VFfDz8tVKVDs/P8+i71TR3DsEwTxg8
aHAgmHA6Gg8mUAXbEnxB1mbtutyhxHdNAVjeZHAQS1B68xKuaEzyvPRx3hCO2/E+
XR/WOXVu8w//ezx32qQ7Ugu4JKzMCDd4IPBSXbxbkQHT7HVUIZUgMCQPp0Yzeqpq
WK55Iw16ov///mWhAaNlbHkZTRq/tzUmuPrV30FlJ0GiSolEGfxbiMbNc2VkIhPL
3vM0xfaJ7R9FV+7LTpN9fOHUFrZWY9tO83JhlcVai84Y3Q8xp59VAYxGaJbGbXYb
Hzuo+iSFo2HSaiwA38JHlPvjiyvresXNZrn7vs7Fc415sYp4Ikbblh+KXEFWBg8f
hmh7xcgm32lCd6Nxm0GQvZt9NOXYjWtyQ7wk+pEavMS1wUso5t+cU47Jj6NhSTGK
3HnnG1uErD3Zeyek5FeI+BRe5t+0cg923lxtCRsx1/IYzkYhm+n8Rt6ttKGcZ/+K
5Ncd15d90NuEmcKbipUeO0h9lQxNGrWRKFzba0GgbBxtLhOKe6oCXmGhJnot/UUp
s/+8yeqKd2eOIvCMdIuBlcYB6QglNcpgiWKPrFFC5QU1XpitKN9cUTfK8byVDmtK
igjES1HI7iQqAKFpziz9Fzq1YTvr0E1kKqU3hVgp/+ftLYVQPkX7rSOg4oldYrjB
JtmeNV4KrKm5cnSwWywL7MK95aqARxDVd7wTl2Oc8zVre4lL7Xy/uYKpcPsR/7Qe
G/FZioek7xWV6qai6m2Raw7v0VhFt68X1nKsvDAJ5DHL8e4/XsiFfoQ=
=QNz7
-----END PGP PUBLIC KEY BLOCK-----
```


## Welche Informationen sind in einem Public Key enthalten?
Die Informationen eines GPG Public Keys werden in einem ASCII Armor, einer Art Panzerung codiert.
Dekodieren wir diesen ASCII Armor enthalten wir Informationen zur verwendeten User ID, Datum und Zeitpunt der 
Schlüsselerzeugung, den verwendete Verschlüsselungs-, Kompressions- und Hashalgorithmen, sowie Beglaubigungen des 
Public Keys:

```
Old: Public Key Packet(tag 6)(525 bytes)
	Ver 4 - new
	Public key creation time - Tue Mar 20 21:09:42 CET 2018
	Pub alg - RSA Encrypt or Sign(pub 1)
	RSA n(4096 bits) - ...
	RSA e(17 bits) - ...
Old: User ID Packet(tag 13)(28 bytes)
	User ID - opzeck <opzeck@systemli.org>
Old: Signature Packet(tag 2)(596 bytes)
	Ver 4 - new
	Sig type - Positive certification of a User ID and Public Key packet(0x13).
	Pub alg - RSA Encrypt or Sign(pub 1)
	Hash alg - SHA512(hash 10)
	Hashed Sub: key flags(sub 27)(1 bytes)
		Flag - This key may be used to certify other keys
		Flag - This key may be used to sign data
		Flag - This key may be used for authentication
	Hashed Sub: preferred symmetric algorithms(sub 11)(4 bytes)
		Sym alg - AES with 256-bit key(sym 9)
		Sym alg - AES with 192-bit key(sym 8)
		Sym alg - AES with 128-bit key(sym 7)
		Sym alg - CAST5(sym 3)
	Hashed Sub: preferred hash algorithms(sub 21)(4 bytes)
		Hash alg - SHA512(hash 10)
		Hash alg - SHA384(hash 9)
		Hash alg - SHA256(hash 8)
		Hash alg - SHA224(hash 11)
	Hashed Sub: preferred compression algorithms(sub 22)(4 bytes)
		Comp alg - ZLIB <RFC1950>(comp 2)
		Comp alg - BZip2(comp 3)
		Comp alg - ZIP <RFC1951>(comp 1)
		Comp alg - Uncompressed(comp 0)
	Hashed Sub: features(sub 30)(1 bytes)
		Flag - Modification detection (packets 18 and 19)
	Hashed Sub: key server preferences(sub 23)(1 bytes)
		Flag - No-modify
	Hashed Sub: issuer fingerprint(sub 33)(21 bytes)
	 v4 -	Fingerprint - 09 10 e6 3c f5 c0 3f a5 48 f2 05 cb 5b ff 96 ac e3 de 0e e9
	Hashed Sub: signature creation time(sub 2)(4 bytes)
		Time - Sun Jun 10 07:42:46 CEST 2018
	Hashed Sub: key expiration time(sub 9)(4 bytes)
		Time - Fri Jan 17 06:42:46 CET 2020
	Sub: issuer key ID(sub 16)(8 bytes)
		Key ID - 0x5BFF96ACE3DE0EE9
	Hash left 2 bytes - cb 56
	RSA m^d mod n(4096 bits) - ...
		-> PKCS-1
Old: Public Subkey Packet(tag 14)(525 bytes)
	Ver 4 - new
	Public key creation time - Tue Mar 20 21:09:42 CET 2018
	Pub alg - RSA Encrypt or Sign(pub 1)
	RSA n(4096 bits) - ...
	RSA e(17 bits) - ...
Old: Signature Packet(tag 2)(572 bytes)
	Ver 4 - new
	Sig type - Subkey Binding Signature(0x18).
	Pub alg - RSA Encrypt or Sign(pub 1)
	Hash alg - SHA512(hash 10)
	Hashed Sub: key flags(sub 27)(1 bytes)
		Flag - This key may be used to encrypt communications
		Flag - This key may be used to encrypt storage
	Hashed Sub: issuer fingerprint(sub 33)(21 bytes)
	 v4 -	Fingerprint - 09 10 e6 3c f5 c0 3f a5 48 f2 05 cb 5b ff 96 ac e3 de 0e e9
	Hashed Sub: signature creation time(sub 2)(4 bytes)
		Time - Sun Jun 10 07:44:35 CEST 2018
	Hashed Sub: key expiration time(sub 9)(4 bytes)
		Time - Fri Mar 20 06:44:35 CET 2020
	Sub: issuer key ID(sub 16)(8 bytes)
		Key ID - 0x5BFF96ACE3DE0EE9
	Hash left 2 bytes - 03 a4
	RSA m^d mod n(4096 bits) - ...
		-> PKCS-1
Old: Public Subkey Packet(tag 14)(525 bytes)
	Ver 4 - new
	Public key creation time - Sun Jun 10 07:43:32 CEST 2018
	Pub alg - RSA Encrypt or Sign(pub 1)
	RSA n(4096 bits) - ...
	RSA e(17 bits) - ...
Old: Signature Packet(tag 2)(1138 bytes)
	Ver 4 - new
	Sig type - Subkey Binding Signature(0x18).
	Pub alg - RSA Encrypt or Sign(pub 1)
	Hash alg - SHA512(hash 10)
	Hashed Sub: issuer fingerprint(sub 33)(21 bytes)
	 v4 -	Fingerprint - 09 10 e6 3c f5 c0 3f a5 48 f2 05 cb 5b ff 96 ac e3 de 0e e9
	Hashed Sub: signature creation time(sub 2)(4 bytes)
		Time - Sun Jun 10 07:43:32 CEST 2018
	Hashed Sub: key flags(sub 27)(1 bytes)
		Flag - This key may be used to sign data
	Hashed Sub: key expiration time(sub 9)(4 bytes)
		Time - Wed Jun 10 07:43:32 CEST 2020
	Sub: issuer key ID(sub 16)(8 bytes)
		Key ID - 0x5BFF96ACE3DE0EE9
	Sub: embedded signature(sub 32)(563 bytes)
	Ver 4 - new
	Sig type - Primary Key Binding Signature(0x19).
	Pub alg - RSA Encrypt or Sign(pub 1)
	Hash alg - SHA512(hash 10)
	Hashed Sub: issuer fingerprint(sub 33)(21 bytes)
	 v4 -	Fingerprint - 71 42 61 51 82 64 e5 fe 08 c9 a2 7a 64 87 cc e9 5d bd 9b 3a
	Hashed Sub: signature creation time(sub 2)(4 bytes)
		Time - Sun Jun 10 07:43:32 CEST 2018
	Sub: issuer key ID(sub 16)(8 bytes)
		Key ID - 0x6487CCE95DBD9B3A
	Hash left 2 bytes - 26 03
	RSA m^d mod n(4091 bits) - ...
		-> PKCS-1
	Hash left 2 bytes - 6e f3
	RSA m^d mod n(4095 bits) - ...
		-> PKCS-1
```

