## Backups

Regelmäßige Backups, also das redundante Speichern von Daten, gehören zu einem vernünftigen Sicherheitskonzept dazu.  
Aktuelle Backups gewährleisten das schnelle und unkomplizierte Fortsetzen der Arbeit im Falle eines Diebstahls,
Defektes, einer Beschlagnahmung oder sonstigem Datenverlust.  
Natürlich müssen die Backups **immer** verschlüsselt sein.  

Die meisten Betriebssysteme bieten bereits Möglichkeiten zum erstellen von Backups auf externen Speichermedien.  

#### Windows

 * [Windows Backup](https://www.heise.de/tipps-tricks/Backup-erstellen-mit-Windows-10-3858841.html#anchor_7)  
 _Zu finden unter:_ `"System und Sicherheit" -> "Sichern und Wiederherstellen" -> "Systemabbild erstellen"`
 
#### macOS

 * [Apple Time Machine](https://support.apple.com/de-de/HT201250)  
 _Zu finden unter:_ `"Systemeinstellungen" -> "Time Machine"`
 
 #### Linux
 
 * [Déjà Dup - Datensicherungswerkzeug](https://wiki.ubuntuusers.de/D%C3%A9j%C3%A0_Dup/)  
 _Zu finden unter:_ `abhängig von der verwendeten Distro`
 
 ### Open Source Backup Tools
  * [Duplicati](https://www.duplicati.com) (Windows, macOS, Linux)