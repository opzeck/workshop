## Tails

> Tails ist ein Live-Betriebssystem, das darauf ausgerichtet ist Ihre Privatsphäre und Anonymität zu bewahren. 
> Es hilft Ihnen dabei, das Internet so gut wie überall und von jedem Computer aus anonym zu nutzen, ohne dabei 
> Spuren zu hinterlassen, sofern Sie dies nicht ausdrücklich wünschen.
> 
> Tails ist ein vollständiges Betriebssystem, das direkt von einem USB-Stick oder einer DVD aus genutzt wird, 
> unabhängig von dem auf dem Computer installierten Betriebssystem. Tails ist Freie Software und basiert 
> auf Debian GNU/Linux.
> 
> Tails beinhaltet verschiedene Programme, die im Hinblick auf die Sicherheit vorkonfiguriert wurden: einen Webbrowser, 
> einen Instant-Messaging-Client, ein E-Mail-Programm, ein Office-Paket, einen Bild- und Audioeditor etc.

> #### Überall nutzen, ohne Spuren zu hinterlassen
>      
> Die Benutzung von Tails auf einem Computer verändert weder das installierte Betriebssystem, noch ist es von 
> diesem abhängig. Es kann also gleichermaßen auf dem eigenen Computer, dem einer Freundin, oder auf einem Computer
> Ihrer örtlichen Bibliothek verwenden werden. Nachdem Sie Tails heruntergefahren haben, kann der Computer wie
> gehabt mit seinem üblichen Betriebssystem starten.  
> Tails ist mit großer Sorgfalt konfiguriert, nicht die Festplatten des Computers zu benutzen, auch dann nicht, 
> wenn Auslagerungsspeicher (swap space) zur Verfügung steht. Der einzige von Tails genutzte Speicher ist 
> der Arbeitsspeicher (RAM), der automatisch gelöscht wird, sobald der Computer herunterfährt.  
> So hinterlassen Sie weder Spuren des Tails-Systems, noch dessen, was Sie auf dem Computer getan haben. 
> Deshalb nennen wir Tails "amnestisch" (engl.: amnesic).
> Dies erlaubt Ihnen auf jedem Computer an sensiblen Dokumenten zu arbeiten und schützt Sie vor 
> Datenwiederherstellung nach dem Herunterfahren. Natürlich können Sie weiterhin ausgewählte Dokumente 
> und Dateien auf einem anderen USB-Stick oder einer externen Festplatte speichern und für eine zukünftige 
> Benutzung mitnehmen.

[https://tails.boum.org](https://tails.boum.org/about/index.de.html)


### 