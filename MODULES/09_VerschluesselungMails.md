# Verschlüsselung I (E-Mails)
`TODO: Verschlüsselung im Webmailer

Um E-Mails bequem zu ver- und entschlüsseln sind in der Regel drei verschiedene Programme notwendig:  

 * [**GnuPG**](22_GnuPG.md)  
 stellt alle für die Verschlüsselung relevanten Funktionen zur Verfügung. Nach der Installation arbeitet das Programm 
 größtenteils im Hintergrund.
 * **E-Mail Client**   
 Der E-Mail Client ist die Schnittstelle mit der ihr in Zukunft auf eure E-Mail Konten zugreift. Ein Abrufen der E-Mails
 über den Webmailer eures Providers ist fortan noch möglich, aber nicht mehr notwendig.
 Es gibt eine große Auswahl an unterschiedlichen E-Mail Clients, [Mozilla Thunderbird](https://www.thunderbird.net/de/
 "https://www.thunderbird.net/") ist auf allen Plattformen kostenlos verfügbar und bietet mit dem Add-on "Enigmail" 
 eine hervorragende Integration von GnuPG.
 * [**Enigmail**](https://www.enigmail.net "https://www.enigmail.net")    
 Enigmail ist als Erweiterung _(Add-on)_ für Mozilla Thunderbird und weitere E-Mail Clients verfügbar. Enigmail stellt
 die Schnittstelle zwischen dem E-Mail Client und der GnuPG Software dar und sorgt dafür, dass dem E-Mail Client 
 alle Funktionen der GnuPG Software zur Verfügung stehen.  
 
 
## Installationsreihenfolge
 
Installiert zuerst die für eure Plattform empfohlene GnuPG Software.  

 * Für Microsoft Windows benötigt ihr [**Gpg4win**](https://gpg4win.de "https://gpg4win.de")
 * Für macOS [**GPGTools**](https://gpgtools.org "https://gpgtools.org")
 * Auf den meisten Linux Distributionen ist GnuPG bereits installiert

Falls ihr noch keinen E-Mail Client verwendet installiert nun [Mozilla Thunderbird](https://www.thunderbird.net/de/
"https://www.thunderbird.net/")  
_(Solltet ihr bereits einen anderen E-Mail Client (Outlook, Apple Mail etc...) verwenden
prüft ob eine (kostenlose) Integration von GnuPG zur verfügung steht. Ist dies der Fall könnt ihr euren E-Mail Client
problemlos weiterverwenden.)_  

Nachdem ihr Mozilla Thunderbird erfolgreich installiert habt könnt ihr das Programm starten und die erscheinenden
Eingabeaufforderungen schließen.

Als nächstes installiert ihr nun Enigmail.  
Enigmail kann bei bestehender Internetverbindung bequem in der Add-on Verwaltung von Mozilla Thunderbrid installiert
werden.  
Klickt dazu rechts oben auf die Schaltfläche mit den horizontal angeordneten drei Streifen (&#8801;) und wählt den
Menüpunkt Add-ons > Add-Ons aus. Enigmail sollte bereits auf der ersten Seite "Add-Ons entdecken" aufgeführt werden.
Ist dies nicht der Fall könnt ihr auf der Seite "Erweiterungen" im Suchformular danach suchen.  
Installiert Enigmail nun durch einen Klick auf die dafür vorgesehene Schaltfläche und stellt anschließend in der Add-on
Verwaltung sicher, dass das Add-on auch aktiviert ist.


## Konfiguration

Als nächstes folgt die Konfiguration von Thunderbird und Enigmail.  
Wählt in Mozilla Thunderbird den Menüpunkt "Konto einrichten: E-Mail, Konto für bestehende E-Mail Adresse einrichten"
und tragt in dem darauf folgenden Dialog die Zugangsdaten eurer E-Mail Adresse ein.  
Als Protokoll könnt ihr **IMAP** auswählen.
TODO: pop3 vs. imap; pop3 in einigen anwendungsfällen sicherer`
Im folgenden Dialog müsst ihr die korrekte Konfiguration überprüfen. Die erforderlichen Angaben sind in der
Dokumentation eures E-Mail Providers zu finden.  

Für Adressen bei [systemli.org](https://www.systemli.org/service/mail.html) müssen folgende Parameter zur korrekten 
Konfiguration gesetzt werden:

```
Server (Posteingang/Ausgang):   mail.systemli.org
Benutzer:                       <username>@systemli.org
Protokoll (Empfang):            IMAP
Port:                           993
Protokoll (Versand):            SMTP
Port:                           587 (StartTLS)
```

Überprüft nun ob die Konfiguration funktioniert indem ihr eine E-Mail an die gerade von euch eingerichtete 
E-Mail Adresse schickt. Erfolgt der Versand und Empfang ohne Probleme, ist eure Konfiguration fehlerfrei.

Als nächstes müssen zwei weitere Einstellungen gesetzt werden.  
Geht dazu in die Einstellungen von Mozilla Thunderbird (mit einem Klick auf die Schaltfläche mit den drei Streifen
(&#8801;) > Einstellungen > Einstellungen) und wählt das Menü "Datenschutz" aus.  

 * Entfernt den Haken bei "Externe Inhalte in Nachrichten erlauben"
 * Wählt den Menüpunkt "Nutzung von S/MIME und Enigmail erzwingen" aus  

 
## GPG Keys erstellen

Wenn eure E-Mail Adresse, Thunderbird und Enigmail konfiguriert sind, könnt ihr damit beginnen ein GPG Schlüsselpaar
für eure E-Mail Adresse zu erstellen. Am leichtesten funktioniert dies über den Enigmail-Einrichtungsassistent.
Um den Einrichtungsassistent zu starten, wählt ihr die Schaltfläche mit den drei Streifen (&#8801;) aus und navigiert
zu Enigmail > Einrichtungsassistent.  

Wählt die Option für fortgeschrittene Benutzer und bestätigt den Dialog.
Im nächsten Fenster solltet ihr eine starke Passphrase definieren. Bevor ihr mit der Einrichtung fortsetzt Sichert eure
Passphrase in einem Passwortmanager oder notiert diese. Nach bestätigen des Dialogs wird euer GPG Schlüsselpaar für euch
erstellt.
Im nächsten Schritt solltet ihr ein Widerrufszertifikat erstellen und an einem sicheren Ort abspeichern.  
Nun habt ihr die Möglichkeit euren Public Key auf einen Schlüsselserver zu laden und diesen damit für andere Leute
zugänglich zu machen. Außerdem könnt ihr einstellen dass euer Public Key automatisch an jede eurer E-Mails angehängt
wird. Diese Einstellung ist gerade zu Beginn deiner GPG-Karriere sinnvoll und erleichtert es verschlüsselte
Unterhaltungen zu initiieren.  

**Die Einrichtung ist nun abgeschlossen, nun könnt ihr eure erste verschlüsselte E-Mail versenden.**