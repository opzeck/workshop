# Nachrichtenverschlüsselung

Die Anforderungen an moderne Nachrichtenverschlüsselung sind aus technischer Sicht in vier Kriterien zu unterteilen.

 * **Vertraulichkeit**  
 _Nur die Empfänger\*innen können die Nachricht entschlüsseln und lesen_
 * **Beglaubigung**  
 _Empfänger\*innen können sicherstellen dass die Nachricht wirklich von den vorgegebenen Absender\*innen kommt_
 * **Abstreitbarkeit**  
 _Nach der Konversation ist die Beglaubigung nichtig_
 * **Folgenlosigkeit**  
 _Falls einem der Gesprächspartner\*innen ein Schlüssel verloren geht hat dies keine Konsequenzen für vorangegangene
 Konversationen_  
 
Leider fehlt es, insbesondere zum [verschlüsseln von E-Mails](09_VerschluesselungMails.md) mit [GnuPG](22_GnuPG.md),
an Software und Lösungen welche all diese Kriterien berücksichtigen.  

Einige [Instant Messenger & Chat-Protokolle](12_MessengerChatprotokolle.md) bieten bei korrekter Anwendung Lösungen
welche diese Kriterien erfüllen.  


# Nachrichtenverschlüsselung &ne; Anonymität

Nachrichtenverschlüsselung verbirgt zwar für außenstehende den Inhalt einer Nachricht, wer mit wem zu welchem Zeitpunkt
kommuniziert wird dabei aber nicht verborgen.  
Während der Kommunikation entstehen zwangsläufig Verbindungsdaten. Wie beim einem Telefonat ist es auch bei Nachrichten
möglich, anhand dieser Daten zu bestimmen wer, von wo, mit wem zu welcher Uhrzeit kommuniziert. Diese Verbindungsdaten 
können dazu genutzt werden um mit einer [Kommunikationsanalyse](14_Kommunikationsanalyse.md) Gruppen und Strukturen
abzubilden.
