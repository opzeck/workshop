# Gruppen- & Aktionshandy

> _Die beste Nachrichten-Verschlüsselung hilft nicht viel wenn die verwendeten Geräte leicht angreifbar, 
> unsicher oder bereits kompromittiert sind.  
> **Das »unsicherste« Gerät bestimmt daher die Sicherheit der ganzen Kommunikation.**_