# Passwörter & Passphrasen  


### Schlechte Passwörter

 * Enthalten personenbezogene Daten  
 * Werden bereits mehrfach verwendet  
 * Bestehen aus weniger als 12 Zeichen  
 * Sind für Fremde leicht zu erraten  
 * Sind unverschlüsselt gespeichert oder versteckt _(Kann auch das Speichern im Web-Browser betreffen!)_
 * Sind in einem Wörterbuch zu finden
 * Wurden bereits geknackt und veröffentlicht
 
_Trifft mindestens eine der Eigenschaften zu:_
 * Werden bereits sehr lange verwendet

   
### Gute Passwörter

 * Bestehen aus **mindestens** 16 Zeichen
 * Sind zufällig generiert
 * Sind einzigartig und werden für nur einen **einzelnen** Login verwendet
 * Werden in einem Passwortmanager gespeichert
 * Werden nicht geteilt
 
_**Masterpasswörter sollten zusätzlich gut zu merken sein**_ 


## (Beispiel-) Konzepte für Passphrasen

Computergeneriert: `r}_S=j{~9Zi\m6~0IQ#9c7QTr<thgz` _(30 Zeichen)_  
Längere Sätze mit Trennzeichen: `Lorem-ipsum-dolor-sit-amet-consetetur-sadipscing-elitr-sed-diam-nonumy-eirmod` _(77 Zeichen)_   
Anfangs- oder Endbuchstaben ganzer Sätze:  `Lidsa,cse,sdnetiule` _(19 Zeichen)_


## Erweiterte Loginverfahren

 * One-time passwords (OTP)
> Ein Einmalkennwort oder Einmalpasswort ist ein Kennwort zur Authentifizierung oder auch Autorisierung.
> Jedes Einmalkennwort ist nur für eine einmalige Verwendung gültig und kann kein zweites Mal benutzt werden.
> Entsprechend erfordert jede Authentifizierung oder Autorisierung ein neues Einmalkennwort.
> Es ist sicher gegen passive Angriffe, also Mithören. Auch Replay-Attacken sind somit unmöglich.
> Gegen das Angriffsszenario Man in the Middle helfen Einmalkennwörter nicht.
> Auch hat der Einsatz von Einmalkennwörtern keinen Einfluss auf die Sicherheit einer Verschlüsselungsmethode.  
 
 * Two-factor authentication (2FA)
> Die Zwei-Faktor-Authentisierung (2FA), häufig auch als Zwei-Faktor-Authentifizierung bezeichnet, bezeichnet den
> Identitätsnachweis eines Nutzers mittels der Kombination zweier unterschiedlicher und insbesondere
> unabhängiger Komponenten (Faktoren). Typische Beispiele sind Bankkarte plus PIN beim Geldautomaten, Fingerabdruck
> plus Zugangscode in Gebäuden, sowie Passphrase und TAN beim Online-Banking.  

 
## Passwortmanager

Ein Passwortmanager erstellt eine verschlüsselte Datenbank in der Passwörter und Login Credentials organisiert und gespeichert werden.
Die Datenbank eines Passwortmanagers wird mit einem starkem "Masterpasswort" verschlüsselt.
Nachdem dieses Masterpasswort eingegeben wurde, erhält man Zugriff auf alle in der Datenbank hinterlegten Daten.  


#### Benefits eines Passwortmanagers

 * Du musst die nur wenige Passwörter merken
 * Leichtes Verwalten von vielen einzigartigen Passwörtern und Logins
 * Automatisches generieren komplexer Passwörter
 * Ein Passwortmanager vergisst nichts
 * [Backups](20_Backups.md) der verschlüsselten Datenbank können auf anderen Computern oder USB-Sticks gespeichert werden
 * Die Datenbank kann auf mehreren Computern verwendet werden. (Es können Probleme während der Synchronisation entstehen!)
 
 
#### Risiken bei der Verwendung eines Passwortmanagers

 * **Bei Verlust der Datenbank oder des Masterpasswords ist kein Zugriff auf deine Informationen mehr möglich!**
 * Falls kein [Backup](20_Backups.md) der Datenbank besteht, gehen bei Defekt oder Verlust der verwendeten Geräte alle Einträge verloren
 * Die Sicherheit der Verschlüsselung ist nur so stark wie das gewählte Masterpasswort


#### Software Passwortmanager

 * **[KeePassX](https://keepassxc.org/)** (macOS, Windows & Linux)
 * **[1Password](https://1password.com/)** (macOS, Windows, Linux, iOS, Android (kostenpflichtig))
 * **[Enpass](https://www.enpass.io/)** (macOS, Windows, Linux, iOS, Android (teils kostenpflichtig))
 * **[macOS Schlüsselbund](https://support.apple.com/de-de/guide/keychain-access/what-is-keychain-access-kyca1083/)** (macOS)
 * **[Seahorse](https://wiki.ubuntuusers.de/Seahorse/)** (Linux)
 

## Angriffsmethoden gegen Passphrasen

 * Leaks von Login Credentials
 * Social Engineeering
 * Bruteforce
 
 
## Deine Login Credentials testen
 
Um herauszufinden ob die eigenen Login Credentials in einem der großen Passwort-Leaks auftauchen kann man 
diese auf Websites mit großen Datenbanken veröffentlichter Zugangsdaten abgleichen.
Solche Tests sollten unbedingt nur bei vertrauenswürdigen und renomierten Anbietern erfolgen.
 
Zwei vertrauenswürdige Websites solcher Services sind:
 * [haveibeenpwned.com](https://haveibeenpwned.com/)
 * [Hasso-Plattner-Institut der Universität Potsdamm](https://sec.hpi.de/ilc/search?lang=de)


