# Gerätesicherheit II (Smartphones & Handys)


Die Sicherheit von Smartphones und Handys wird durch die selben Faktoren wie 
bei den [Computern](02_GeraetesicherheitPC.md) ermittelt. Jedoch müssen einige gerätespezifische Faktoren 
berücksichtigt werden.   

Im Unterschied zu den meisten Computern enthalten Smartphones zusätzlich eine Vielzahl an Sensoren und Chips um 
Funktionen wie das mobile Telefonieren, die Standortbestimmung und Navigation und das schnelle Entsperren des Geräts zu
ermöglichen.  
Ein weiterer Unterschied zu herkömmlichen Computern und Laptops ist unser Nutzungsverhalten: während Desktop Computer 
meist am selben Ort verwendet werden, tragen wir unsere Smartphones die meiste Zeit nah an unserem Körper.
In Kombination mit den vorhandenen Sensoren und Chips ergeben sich neue Möglichkeiten zum beobachten, abhören, orten und 
dem erstellen von (langzeit) Bewegungsprofilen des Users.  

Um die Unterscheiden zu herkömmlichen PCs übersichtlicher hervorzuheben, unterscheiden wir wieder zwischen 
**Gerätesicherheit** und **Privatsphäre**.  


## Gerätesicherheit

##### iOS & Android

 * Die Entsperrmethoden sind weitaus primitiver als bei Computern. Muster, vierstellige Pincodes oder biometrische 
 Entsperrmethoden (Fingerabdruck, Face ID) sind leicht zu umgehen oder erraten.  
 
 * Das ständige eingeben einer ausreichend sicheren Passphrase (mindestens 16 Zeichen) schränkt die usability enorm ein.
 
 * Smartphones erhalten weitaus seltener und weniger Sicherheitsupdates als Computer.  
 
 * Smartphones sind die meiste im Standby Modus, während dem Betrieb sind die Daten auf dem Geräte trotz aktivierter 
 Geräteverschlüsselung nicht verschlüsselt. (evtl. ist dies bei iOS inzwischen anders)
 
 
##### Android

 * Die meisten Android-Smartphones werden mit veralteten Bettriebssystemen ausgeliefert, 
 schlimmstenfalls sind (Sicherheits-) Updates überhaupt nicht möglich. Ein Upgrade auf die aktuellste Android-Version 
 ist in den meisten Fällen nicht vorgesehen.  
 
 * Die meisten Android-Smartphones werden mit einer durch den Hersteller modifizierten/manipulierten Android-Version 
 ausgeliefert. Häufig können Apps des Herstellers nicht vom Gerät entfernt werden.  
 
 * Nach dem aktivieren der Systemverschlüsselung bleibt eine eingesetzte Speicherkarte in den meisten 
 Fällen unverschlüselt.  
 
 * Apps aus dem Google Play Store durchlaufen keine nennenswerte Sicherheitskontrolle, Apps aus alternativen App Stores
 oder Apps die als .apk-Datei von Websites geladen werden können durchlaufen überhaupt keine Sicherheitskontrolle.  
 
 * App-Berechtigungen sind komplex und undurchsichtig, die meisten Applikationen verlangen mehr Zugriffsrechte als 
 notwendig.
 
## Privatsphäre
 
 * Während dem Betrieb werden die Daten der verbauten Sensoren auf dem System und in den Apps gespeichert. 
 Diese informationen können (forensisch) ausgewertet werden. Anhand dieser Daten können die unterschiedlichsten Profile 
 erstellt und das Nutzungsverhalten analysiert werden. Dies geschiet täglich u.A. um personenbezogene Werbung auszuliefern.
 Firmen wie Facebook oder Google nutzen diese Daten und kombinieren sie mit den Informationen die bereits in den 
 jeweiligen Anwendungen/Accounts gespeichert sind.  
 
 * Das deaktivieren von Funktionen wie WLAN, GPS oder dem Mikrofon geschiet per Software, eine Garantie dass diese 
 auch wirklich ausgeschaltet sind gibt es nicht.  
 
 * Viele Apps verlangen für ihre reguläre Funktion bereits Berechtigungen die für eine Überwachung geeignet wären.
 
 * Erfasste Metadaten (z.B. Detailierte Standortangabe) können unbeabsichtigt in Fotos oder Social Media Beiträgen 
 gespeichert und veröffentlicht werden.
 
 ##### Android
 * Android ist zwar freie Software und basiert auf Linux, jedoch wurde das Smartphone-Betriebssyste von Google gegründet 
 und wird seitdem auch maßgeblich von Google entwickelt. Die verschiedenen services von Google sind so weit in das 
 System integriert, dass es nicht ohne weiteres möglich ist ein Android zu betreiben ohne dabei Daten an Google zu 
 übermitteln. Ein Versuch Android von Google zu befreien ist in dieser 
 [Anleitung von systemli.org](https://www.systemli.org/de/2018/04/29/google-freies-android.html) dokumentiert.
 
 ##### iOS
 
 * Eine Appübergreifende verwendung des anonymisierungsdienstes [TOR](08_TheOnionRouting.md) ist technisch nicht möglich.