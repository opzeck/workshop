# Gerätesicherheit I (Computer)

Wir unterscheiden zunächst zwischen Gerätesicherheit und Privatsphäre. Inhaltlich überschneiden sich beide 
Themen häufig, können aber grob in [Verschlüsselung](10_VerschluesselungOS.md) (Gerätesicherheit) und 
Anonymität (Privatsphäre/Privacy) aufgeteilt werden.

Um die Vertraulichkeit der auf deinen Geräten gespeicherten Daten bei Diebstahl, Verlust, Hacks oder 
Beschlagnahmung zu gewährleisten, ist eine Verschlüsselung der kompletten Geräte, mindestens aber der gespeicherten 
Daten notwendig. Anderenfalls sind die Daten bei einem physischem Zugriff auf das jeweilige Gerät leicht auslesbar.  
 
  Neben einer Verschlüsselung hängt die Sicherheit der Geräte von weiteren unterschiedlichen Faktoren ab.


### Sicherheitsfaktoren

 * Verwendete Verschlüsselung & Authentifizierungsmethode
 * Zugänglichkeit des jeweiligen Geräts
 * Verwendete Hardware
 * Verwendetes Betriebssystem
   * Aktualität des verwendeten Betriebssystem
 * Verwendete Software
   * Aktualität der verwendeten Software
 * Verhalten der User 


## Verschlüsselung, Authentifizierung & Zugänglichkeit
   
Computer und ihre Betriebssysteme sind im Auslieferungszustand nicht verschlüsselt. Die meisten modernen Betriebssysteme
enthalten bereits Möglichkeiten die Systeme zu verschlüsseln, in einigen Fällen muss dafür weitere Software 
auf dem Gerät installiert werden.

Um gezielt Daten deines Computers auszulesen oder Schadsoftware auf diesem zu installieren wird ist den meisten Fällen 
ein physischer Zugriff auf das Gerät notwendig. Geräte die häufig unbeaufsichtigt und Öffentlichkeit  
herumstehen sind besonders gefährdet.  
**_Die Verschlüsselung des Betriebssystems wird nach dem Einschalten und mit dem Entsperren des Geräts aufgehoben.
Im Standby befindliche Geräte sind also nicht verschlüsselt, vermeide daher nach Möglichkeit deine Geräte über Nacht 
oder bei längerer Abwesenheit eingeschaltet zu lassen._**  


## Hardware, Betriebssysteme, Software & (Sicherheits-) Updates

Die Hersteller der Geräte und Betriebssysteme legen unterschiedlich viel Wert auf security und privacy. 
Während Apple z.B. nachgesagt wird ist einigermaßen viel Wert auf security und privacy zu legen, 
eilt Microsoft seit jeher ein zweifelhafter Ruf bei diesen Punkten voraus. Bei Windows 10 scheint es derzeit z.B. 
nicht möglich das übermitteln von Informationen zum Nutzungsverhalten, sog. Telemetriedaten an Microsoft server zu 
unterbinden. Dies hat zwar keinen direkten Einfluss auf die security des Betriebssystem, stellt allerdings einen 
erheblichen Eingriff in die Privatsphäre der Nutzer*innen dar.  

Einer der wichtigsten Aspekte der Gerätesicherheit ist die aktualität der installierten Software.  
**_Es sollte stets die aktuelle Version des Betriebssystems installiert sein, bestenfalls geschiet dies automatisch. 
Dies gewährleistet die Verfügbarkeit der aktuellen (Sicherheits-) Updates._**
Auch die verwendeten Applikationen sollten stets auf dem neusten Stand gehalten werden. Dies betrifft insbesondere 
Software welche direkt auf das Internet zugreift. (Web-Browser, Browser-Plugins(!), E-Mail Clients etc...)  
_Auf älteren Geräten ist es unter Umständen nicht möglich die aktuelle Version von Windows oder macOS zu installieren!_  

Ein weiterer wichtiger Aspekt ist die herkunft der verwendeten Software. Software sollte von den 
Websites der Entwickler\*innen oder des jeweiligen App Stores bezogen werden. Um manipulierte Software zu entdecken 
sollten, falls möglich, nach dem Download die Prüfsummen und Signaturen der Software mit denen der Entwickler*innen 
verglichen werden.  

**In jedem Fall sollte vermieden werden:**

 * Software aus unbekannten Quellen zu downloaden und installieren
   * ...z.B. durch Drittanbieter wie chip.de, giga.de, computerbild.de [...]
   * "Raubkopien" kostenpflichter Software o.Ä. zu installieren  
 * Programme zum umgehen der Aktivierung kostenpflichtiger Software zu installieren


## Verhalten des Users
   
 Am häufigsten wird Schadsoftware durch aktive Mitarbeit des Users auf dem Gerät installiert. Dies geschiet z.B. durch:
 
 * Öffnen unbekannter E-Mail Anhänge.
   * u.A. .docx _(Microsoft Word)_, .ppt, .pptx & .pptm _(alle Powerpoint)_, .exe _(Windows)_ oder PDF-Dateien
 * Werbung auf Filesharing oder (illegaler) Video-Streaming Websites (boerse.to, kinox.to)
 * Illegale Raubkopien (Filme, Musik, Programme, Bilder, E-Books, etc...)
 * Web Browser Plugins