# OPZECK WORKSHOP 12.05.2019  


 1. [Was ist Opsec?](MODULES/19_Opsec.md)
 1. [Passwörter& Passphrasen](MODULES/01_PasswoerterPassphrasen.md)
 1. [Passwortmanagement](MODULES/01_PasswoerterPassphrasen.md)
 1. [Linke Tech-Infrastruktur](MODULES/16_LinkeInfrastruktur.md)
 1. [Gerätesicherheit Smartphones](MODULES/03_GeraetesicherheitSmartphonesHandys.md) und [Computer](MODULES/02_GeraetesicherheitPC.md)
 1. [Internet](MODULES/05_Internet.md) und [Sicherheit im Internet](MODULES/06_SicherheitImInternet.md)
 1. [Nachrichtenverschlüsselung](MODULES/24_NachrichtenVerschluesselung.md)
 1. [GnuPG](MODULES/22_GnuPG.md)
 1. [E-Mails verschlüsseln](MODULES/09_VerschluesselungMails.md)
 1. [Instant Messenger](MODULES/12_MessengerChatprotokolle.md)


## [MODULES](MODULES)  
_Module aus denen der Workshop zusammengestellt wird._

 1. [Passwörter und Passphrasen](MODULES/01_PasswoerterPassphrasen.md)  
 1. [Gerätesicherheit I (Computer)](MODULES/02_GeraetesicherheitPC.md)  
 1. [Gerätesicherheit II (Smartphones & Handys)](MODULES/03_GeraetesicherheitSmartphonesHandys.md)  
 1. [Welche Software verwende ich eigentlich?](MODULES/04_WelcheSoftwareVerwendeIch.md)  
 1. [Wie funktioniert das Internet?](MODULES/05_Internet.md)  
 1. [Sicherheit im Internet](MODULES/06_SicherheitImInternet.md)  
 1. [GSM, UMTS, LTE & WLAN Netzwerke](MODULES/18_Netzwerke.md)  
 1. [Anonymität im Internet, Metadaten & Datensparsamkeit](MODULES/07_AnonymitaetImInternet.md)  
 1. [The Onion Routing, TorBrowser & Whonix](MODULES/08_TheOnionRouting.md)  
 1. [Verschlüsselung I (E-Mails)](MODULES/09_VerschluesselungMails.md)  
 1. [Verschlüsselung II (Betriebssysteme)](MODULES/10_VerschluesselungOS.md)  
 1. [Verschlüsselung III (Dateien)](MODULES/11_VerschluesselungDateien.md)  
 1. [Instant Messenger & Chatprotokolle](MODULES/12_MessengerChatprotokolle.md)  
 1. [Arbeiten in Teams](MODULES/13_ArbeitenInTeams.md)  
 1. [Kommunikationsanalayse](MODULES/14_Kommunikationsanalyse.md)  
 1. [Gruppen- & Aktionshandy](MODULES/15_Aktionshandy.md)  
 1. [Linke, Datensparsame Infrastruktur](MODULES/16_LinkeInfrastruktur.md)  
 1. [Software & Tools](MODULES/17_SoftwareToolTipps.md)  
 1. [Was ist Opsec?](MODULES/19_Opsec.md)
 1. [Backups](MODULES/20_Backups.md)
 1. [Tails](MODULES/21_Tails.md)
 1. [GnuPG & PGP](MODULES/22_GnuPG.md)
 1. [Virtuelle Maschinen](MODULES/23_VirtualMachines.md)
 1. [Nachrichtenverschlüsselung](MODULES/24_NachrichtenVerschluesselung.md)

## [IMG](IMG)
_Grafiken für Slides und Anleitungen._

 * [DEMO](IMG/DEMO)  
   * [KeePassX](/IMG/DEMO/KeePassX)
   * [Thunderbrid & Enigmail](IMG/DEMO/LinkeInfrastruktur)
   * [Linke Infrastruktur](IMG/DEMO/ThunderbirdEnigmail)
 * [Kommunikationsanalyse](IMG/Kommunikationsanalyse)
 
 
## [SLIDES](SLIDES)
_Fertige Slides._

 * [Cryptoparty Hamburg](SLIDES/CryptopartyHH)  

 
## [TXT](TXT)
_Mailings, Textstücke & Notizen._
 * [MAILINGS](TXT/MAILINGS)
 * [Notes.md](TXT/Notes.md)
 
 
## [Links](Links.md)
_Relevante Links & Artikel (tbc)._

 * [BitLocker - Windows Laufwerksverschlüsselung (ab Windows 10)](https://docs.microsoft.com/de-de/windows/device-security/bitlocker/bitlocker-overview\ "https://docs.microsft.com")  
 * [Tor Browser (für Mac, Windows, Linux)](https://www.torproject.org/download/download-easy.html.en "https://www.torproject.org/")  
 * [Thunderbird E-Mail client (für Mac, Windows, Linux)](https://www.mozilla.org/de/thunderbird/ "https://www.mozilla.org/")  
 * [GnuPG Dokumentation](https://www.gnupg.org/index.html "https://www.gnupg.org/")  
 * [GPG4Win -  GPG-Software (für Windows)](https://www.gpg4win.org/download.html "https://www.gpg4win.org/")  
 * [GPG-Tools - GPG-Software (für macOS)](https://gpgtools.org/ "https://gpgtools.org/")  
 * [Sofwarevorschläge für Jabber clients](https://www.systemli.org/service/xmpp.html "https://www.systemli.org/")  
 * [Tipps für sicherere Kommunikation](https://wiki.systemli.org/howto/tipps_fuer_sicherere_kommunikation "https://wiki.systemli.org/")  
 * [Jabber-Groupchat mir Pidgin einrichten](https://wiki.systemli.org/howto/jabber-groupchat "https://wiki.systemli.org/")
 * [Deutschsprachiges GnuPG Tutorial (CLI)](https://wiki.ubuntuusers.de/GnuPG/ "https://wiki.ubuntuusers.de") 